<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('date_of_birth')->nullable($value = true);
            $table->string('contact');
            $table->text('address');
            $table->string('city');
            $table->string('landmark');
            $table->string('state');
            $table->string('pincode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('date_of_birth');
            $table->dropColumn('contact');
            $table->dropColumn('address');
            $table->dropColumn('city');
            $table->dropColumn('landmark');
            $table->dropColumn('state');
            $table->dropColumn('pincode');
        });
    }
}
