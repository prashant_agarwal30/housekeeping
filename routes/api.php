<?php

use App\Http\Controllers\GroupController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\RightController;
use App\Http\Controllers\ServiceChargeController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\ServiceRequestController;
use App\Http\Controllers\UserController;
use App\Models\Property;
use App\Models\ServiceRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register', [UserController::class, 'registration']);
Route::post('/login', [UserController::class, 'login']);




Route::group(['middleware' => 'auth:api'], function(){
    Route::get('/details', [UserController::class, 'details']);
    Route::get('/user_properties/{limit}', [UserController::class, 'getProperties']);
    Route::get('/get_staff', [UserController::class, 'getStaff']);
    Route::get('/get_owner', [UserController::class, 'getOwner']);
    Route::put('/user/{id}', [UserController::class, 'update']);
    Route::get('/user', [UserController::class, 'index'])->middleware('isAdmin');
    Route::delete('/user/{id}', [UserController::class, 'destroy']);
    Route::put('/block_user/{id}', [UserController::class, 'blocked']);
    Route::put('/unblock_user/{id}', [UserController::class, 'unblocked']);

    Route::get('/services', [ServiceController::class, 'index']);
    Route::get('/services/{id}', [ServiceController::class, 'show']);
    Route::post('/services', [ServiceController::class, 'store']);
    Route::put('/services/{id}', [ServiceController::class,'update']);
    Route::delete('/services/{id}', [ServiceCOntroller::class, 'destroy']);

    Route::post('/properties', [PropertyController::class, 'store']);
    Route::get('/properties', [PropertyController::class, 'index']);
    Route::get('/properties/{id}', [PropertyController::class, 'show']);
    Route::put('/properties/{id}', [PropertyController::class,'update']);
    Route::delete('/properties/{id}', [PropertyController::class, 'destroy']);

    Route::post('/service_requests', [ServiceRequestController::class, 'store']);
    Route::put('/service_requests/{id}', [ServiceRequestController::class, 'update']);
    Route::get('/services_by_propertiesId/{id}', [ServiceRequestController::class, 'getServicesByPropertiesId']);
    Route::get('/properties_by_servicesId/{id}', [ServiceRequestController::class, 'getPropertiesByServicesId']);
    Route::get('/service_request_data', [ServiceRequestController::class, 'getServiceRequestData']);
    Route::get('/all_service_request_data', [ServiceRequestController::class, 'getAllServiceRequestData']);
    Route::get('/user_services', [ServiceRequestController::class, 'getUserServices']);
    
    Route::post('/service_charge', [ServiceChargeController::class, 'store']);
    Route::get('/service_charge', [ServiceChargeController::class, 'getServiceCharge']);
    Route::delete('/service_charge/{id}', [ServiceChargeController::class, 'destroy']);
    Route::put('/service_request/{id}', [ServiceRequestController::class, 'updateWithoutCheck']);



    Route::get('/payment/{id}', [ServiceRequestController::class, 'payment']);

    Route::post('/groups', [GroupController::class, 'store'])->middleware('isAdmin');
    Route::get('/groups', [GroupController::class, 'index'])->middleware('isAdmin');
    Route::put('/groups/{id}', [GroupController::class, 'update'])->middleware('isAdmin');
    Route::delete('/groups/{id}', [GroupController::class, 'destroy'])->middleware('isAdmin');
    Route::post('/group_users', [GroupController::class, 'store_users'])->middleware('isAdmin');
    Route::get('/group_users/{id}', [GroupController::class, 'getUsersByGroupId'])->middleware('isAdmin');
    Route::get('/user_groups/{id}', [GroupController::class, 'getGroupsByUserId'])->middleware('isAdmin');


    Route::post('/rights', [RightController::class, 'store'])->middleware('isAdmin');
    Route::get('/rights', [RightController::class, 'index'])->middleware('isAdmin');
    Route::put('/rights/{id}', [RightController::class, 'update'])->middleware('isAdmin');
    Route::delete('/rights/{id}', [RightController::class, 'destroy'])->middleware('isAdmin');
    Route::get('/group_rights/{id}', [RightController::class, 'getRightsByGroupId'])->middleware('isAdmin');
    Route::get('/user_rights', [UserController::class, 'userRights']);


    Route::get('/statistics_data/{type}', [UserController::class, 'statisticsData']);




    Route::post('/logout', [UserController::class, 'logout']);
    
});