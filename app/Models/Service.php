<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    public $table = 'services';

    protected $fillable = [
        'name',
        'charge'
    ];

    public function properties() {
        return $this->belongsToMany(Property::class, 'service_requests', 'service_id', 'properties_id');
    }
}
