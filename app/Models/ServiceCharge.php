<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceCharge extends Model
{
    use HasFactory;
    public $table = 'service_charges';

    protected $fillable = [
        'service_id',
        'user_id',
        'charge'
    ];
}
