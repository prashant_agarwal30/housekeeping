<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceRequest extends Model
{
    use HasFactory;
    public $table = 'service_requests';

    protected $fillable = [
        'status',
        'service_id',
        'properties_id',
        'assigned_user',
        'date',
        'time',
        'amount',
        'payment_status'
    ];

    public function getPayment() {
        return $this->hasOne(Payment::class);
    }
}
