<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Right extends Model
{
    use HasFactory;
    public $table = 'rights';

    protected $fillable = [
        'name',
        'code'
    ];

    public function groups() {
        return $this->belongsToMany(Group::class, 'group_right', 'right_id', 'group_id');
    }

}
