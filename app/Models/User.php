<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// use Laravel\Sanctum\HasApiTokens;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'fname',
        'lname',
        'email',
        'password',
        'type',
        'contact',
        'address',
        'city',
        'landmark',
        'state',
        'pincode',
        'date_of_birth',
        'archived',
        'blocked'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function aauthAcessToken(){
        return $this->hasMany('\App\Models\OauthAccessToken');
    }

    public function getProperties() {
        return $this->hasMany('\App\Models\Property');
    }

    public function groups() {
        return $this->belongsToMany(Group::class, 'user_group', 'user_id', 'group_id');
    }

    
}
