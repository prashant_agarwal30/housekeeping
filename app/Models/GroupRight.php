<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupRight extends Model
{
    use HasFactory;
    public $table = 'group_right';
}
