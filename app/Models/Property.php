<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;
    public $table = 'properties';

    protected $fillable = [
        'address',
        'name',
        'status',
        'user_id',
        'pincode'
    ];

    public function services() {
        return $this->belongsToMany(Service::class, 'service_requests', 'properties_id', 'service_id');
    }
}
