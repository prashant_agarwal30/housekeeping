<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    public $table = 'groups';

    protected $fillable = [
        'name',
        'description'
    ];


    public function users() {
        return $this->belongsToMany(User::class, 'user_group', 'group_id', 'user_id');
    }

    public function rights() {
        return $this->belongsToMany(Right::class, 'group_right', 'group_id', 'right_id');
    }

}
