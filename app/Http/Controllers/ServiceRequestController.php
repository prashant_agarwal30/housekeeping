<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Property;
use App\Models\Service;
use App\Models\ServiceCharge;
use App\Models\ServiceRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceRequestController extends Controller
{
    
    public function store(Request $request)
    {
        $request->validate([
            'status'=>'required',
            'service_id'=>'required',
            'properties_id'=>'required',
            'assigned_user'=>'required',
            'date'=>'required',
            'time'=>'required' 
        ]);
        
        $property = Property::find($request->properties_id);
        $loggedInUser = Auth::user();
        if($property) {
            if($property->user_id !== $loggedInUser->id) {
                return response()->json(['message'=>'restricted access to property', 'data'=>'null'], 200); 
            }
            $serviceCharge = ServiceCharge::where(['service_charges.service_id'=>$request->service_id,
                                                'service_charges.user_id'=>$request->assigned_user])->get()->first();
            if($serviceCharge) {                                       
                $amount = $serviceCharge->charge;
                // $serviceRequests = ServiceRequest::create($request->all());
                $serviceRequests = new ServiceRequest();
                $serviceRequests->status = $request->status;
                $serviceRequests->service_id = $request->service_id;
                $serviceRequests->properties_id = $request->properties_id;
                $serviceRequests->assigned_user = $request->assigned_user;
                $serviceRequests->date = $request->date;
                $serviceRequests->time = $request->time;
                $serviceRequests->payment_status = 0;
                $serviceRequests->amount = $amount;
                $serviceRequests->status = $request->status;
                $serviceRequests->save();
                
                $payment = new Payment();
                $payment->amount = $amount;
                $payment->payment_status = 0;
                $payment->payment_mode = 1;
                $payment->service_request_id = $serviceRequests->id;
                $payment->save();
                
                return ServiceController::responseFormat($serviceRequests, "serviceRequest Created Successfully", "serviceRequest Not Created");
            } else {
                return response()->json(['message'=>"no entry present for user_id and service_id in service charge table", 'data'=>'null'], 200); 
            }
        } else {
            return response()->json(['message'=>"property for id " . $request->properties_id . " not present", 'data'=>'null'], 200); 
        }
    }

   
    public function update(Request $request, $id)
    {
        $loggedInUser = Auth::user();
        $loggedInUserType = $loggedInUser->type;
        $serviceRequest = ServiceRequest::find($id);
        if($serviceRequest) {
            $currentStatus = $serviceRequest->status;
            $updateStatus = $request->status;
            if($loggedInUserType == 0) {
                //staff
                if($currentStatus == 0) {
                    if($updateStatus == 5) {
                        return response()->json(['message'=>"not allowed", 'data'=>'null'], 200);
                    }
                } else if($currentStatus == 1) {
                    if($updateStatus == 0 || $updateStatus == 2 || $updateStatus == 5) {
                        return response()->json(['message'=>"not allowed", 'data'=>'null'], 200);
                    }
                } else if($currentStatus == 3) {
                    if($updateStatus != 4) {
                        return response()->json(['message'=>"not allowed", 'data'=>'null'], 200);
                    }
                } else if($currentStatus == 2 || $currentStatus == 4 || $currentStatus == 5) {
                    return response()->json(['message'=>"not allowed", 'data'=>'null'], 200);
                }
            }
            if($loggedInUserType == 1) {
                //owner
                if($currentStatus == 0 || $currentStatus == 1) {
                    if($updateStatus != 5 ) {
                        return response()->json(['message'=>"not allowed", 'data'=>'null'], 200);
                    }
                }

                if($currentStatus == 2 || $currentStatus == 3 || $currentStatus == 4 || $currentStatus == 5) {
                    return response()->json(['message'=>"not allowed", 'data'=>'null'], 200);
                }
            }
            
            $serviceRequest->update($request->all()); 
            return $this::responseFormat($serviceRequest, "serviceRequest with id " . $id . " updated successfully", "serviceRequest with id " . $id . " updation failed");
            
        } else {
            return response()->json(['message'=>"serviceRequest not present for the given id", 'data'=>'null'], 200);
        }
    }

    public function updateWithoutCheck(Request $request, $id) {
        $serviceRequest = ServiceRequest::find($id);
        if($serviceRequest) {
            $serviceRequest->update($request->all());
            return $this::responseFormat($serviceRequest, "serviceRequest with id " . $id . " updated successfully", "serviceRequest with id " . $id . " updation failed");
        }
    }

    
    public function getServicesByPropertiesId($id) {
        $property = Property::find($id);
        if($property) { 
            $services = $property->services;
            return response()->json(['message'=>" Services for properties_id fetched successfully ", 'data'=>$services], 200);
        }
        return response()->json(['message'=>"property not present for the given id", 'data'=>'null'], 200);
    }

    public function getPropertiesByServicesId($id) {
        $Service = Service::find($id);
        if($Service) {
            $properties = $Service->properties;
            return response()->json(['message'=>" Properties fo service_id fetched successfully ", 'data'=>$properties], 200);
        }
        return response()->json(['message'=>"service not present for the given id", 'data'=>'null'], 200);
    }

    public function payment($service_request_id) {
        $payment = ServiceRequest::find($service_request_id)->getPayment()->get();
        return $payment;
    }

    public function getServiceRequestData() {
        $loggedInUser = Auth::user();
        $loggedInUserId = $loggedInUser->id;
        $data = ServiceRequest::join('properties', 'properties.id', '=', 'service_requests.properties_id')
        ->join('services', 'services.id', '=','service_requests.service_id' )
        ->join('payments', 'payments.service_request_id', '=', 'service_requests.id')
        ->join('users', 'users.id', '=', 'service_requests.assigned_user')
        ->orderBy('service_requests.created_at', 'DESC')
        ->where(['properties.user_id' => $loggedInUserId, 'properties.archive'=> 0])
        ->select('service_requests.id as service_requests_id', 'service_requests.created_at as service_requests_created_date', 'service_requests.date as request_date','service_requests.time as request_time','users.fname as staff_fname','users.lname as staff_lname','service_requests.status as service_request_status' ,'properties.name as property_name', 'services.name as service_name', 'payments.amount as payment_amount', 'payments.payment_status as payment_status')
        ->paginate(10);
        
        return response()->json($data);
    }

    public function getAllServiceRequestData() {
        $loggedInUser = Auth::user();
        $data = ServiceRequest::join('properties', 'properties.id', '=', 'service_requests.properties_id')
        ->join('services', 'services.id', '=','service_requests.service_id' )
        ->join('payments', 'payments.service_request_id', '=', 'service_requests.id')
        ->join('users', 'users.id', '=', 'service_requests.assigned_user')
        ->orderBy('service_requests.created_at', 'DESC')
        ->select('service_requests.id as service_requests_id', 'service_requests.created_at as service_requests_created_date', 'service_requests.date as request_date','service_requests.time as request_time','users.fname as staff_fname','users.lname as staff_lname','service_requests.status as service_request_status' ,'properties.name as property_name', 'services.name as service_name', 'payments.amount as payment_amount', 'payments.payment_status as payment_status')
        ->paginate(10);
        
        return response()->json($data);
    }

    public function getUserServices() {

        $loggedInUser = Auth::user();
        $loggedInUserId = $loggedInUser->id; 
        $data = ServiceRequest::join('properties', 'properties.id', '=', 'service_requests.properties_id')
        ->join('services', 'services.id', '=','service_requests.service_id' )
        ->join('payments', 'payments.service_request_id', '=', 'service_requests.id')
        ->join('users', 'users.id', '=', 'properties.user_id')
        ->orderBy('service_requests.created_at', 'DESC')
        ->where(['service_requests.assigned_user'=> $loggedInUserId,'properties.archive' => '0'])
        ->select('service_requests.id as service_requests_id','service_requests.status as service_requests_status','service_requests.created_at as service_request_created_date','properties.name as property_name','properties.address as property_address','users.fname as owner_name','users.contact as owner_contact','service_requests.date as request_date','service_requests.time as request_time' ,'services.name as service_name', 'payments.amount')
        ->paginate(10);

        return response()->json($data);
         
    }
}
