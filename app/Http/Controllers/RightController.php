<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupRight;
use App\Models\Right;
use Illuminate\Http\Request;

class RightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $right = Right::paginate(10);
        return response()->json($right);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "code"=>"required"
        ]);
        $input = $request->all();
        if(!empty($input)) {
            $right = new Right();
            $right->name = isset($input['name']) ? $input['name'] : '';
            $right->code = isset($input['code']) ? $input['code'] : '';
            $right->save();

            return $this::responseFormat($right, "right saved successfully", "saving failed");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!empty($request->all())) {
            $right = Right::find($id);
            if($right) {
                $right->update($request->all());
                return $this::responseFormat($right, "right with id " . $id . " updated successfully", "right with id " . $id . " updation failed");
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id > 0) {
            $res=Right::where('id',$id)->delete();
            if($res) {
                return response()->json(['success'=>true,'message'=>"right deleted successfully"], 200);
            } else {
                return response()->json(['success'=>false,'message'=>"right deletion failed"], 200);
            }
        }
    }

    // public function groupRights(Request $request) {
    //     $input = $request->all();
    //     if(!empty($input)) {
    //         $groupId = isset($input['group_id']) ? $input['group_id'] : '';
    //         $rights = isset($input['rights']) ? $input['users'] : '';

    //         foreach($rights as $right) {
    //             $groupRight = new GroupRight();
    //             $groupRight->group_id = $groupId;
    //             $groupRight->right_id = $right;
    //             $groupRight->save;
    //         }
    //     }
    // }

    public function getRightsByGroupId($id) {
        $group = Group::find($id);
        if($group) {
            $users = $group->rights;
            return response()->json(['message'=>" rights for group_id fetched successfully ", 'data'=>$users], 200);
        } else {
            return response()->json(['message'=>"group not present for the given id", 'data'=>'null'], 200);
        }
    }
}
