<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function responseFormat($service, $successMsg, $errorMsg) {
        if($service === null) {
            return response()->json(['success'=>false, 'message'=>$errorMsg, 'data'=>$service], 200); 
        }
        return response()->json(['success'=>true,'message'=>$successMsg, 'data'=>$service], 200); 
    }
}
