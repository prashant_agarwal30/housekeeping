<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\ServiceCharge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceChargeController extends Controller
{
  

    public function store(Request $request)
    {
        $request->validate([
            'service_id'=>'required',
            'user_id'=>'required',
            'charge'=>'required'
        ]);
        $loggedInUser = Auth::user();
        if($loggedInUser->type == 0) {
            $duplicateServiceCharge = ServiceCharge::where(['service_charges.service_id'=> $request->service_id, 'service_charges.user_id'=>$request->user_id, 'service_charges.archive'=>0])->first();
            if($duplicateServiceCharge) {
                //update
                $duplicateServiceCharge->charge = $request->charge;
                $duplicateServiceCharge->save();
                return response()->json(['success'=>true,'message'=>'ServiceCharge updated successfully', 'data'=>$duplicateServiceCharge], 200);
            } else {
                //save
                $serviceCharge = ServiceCharge::create($request->all());
                return response()->json(['success'=>true,'message'=>'ServiceCharge created successfully', 'data'=>$serviceCharge], 200);
            }
        }
        return response()->json(['success'=>false, 'message'=>'only staff can store/update service charge data'], 202);
    }



    public function destroy($id)
    {
        $loggedInUser = Auth::user();
        if($loggedInUser->type == 0) {
           $serviceCharge = ServiceCharge::find($id);
           if($serviceCharge) {
               // archive 1 signifies deleted
                $serviceCharge->archive = 1;
                $serviceCharge->save();
                return $this::responseFormat($serviceCharge, "serviceCharge with id " . $id . " deleted successfully", "serviceCharge with id " . $id . " deletion failed");
           } else {
                return response()->json(['success'=>false, 'message'=>'ServiceCharge not present for given id'], 202); 
           }
        }
        return response()->json(['success'=>false, 'message'=>'only staff can delete service charge data'], 202);
    }       

    public function getServiceCharge() {
        $loggedInUser = Auth::user();
        if($loggedInUser->type == 0) {
            $loggedInUserId = $loggedInUser->id;
            $data = ServiceCharge::join('services', 'services.id', '=', 'service_charges.service_id')
            ->where(['service_charges.user_id'=>$loggedInUserId, 'service_charges.archive'=>0])
            ->select('service_charges.id as service_charge_id', 'services.name as service_name', 'service_charges.charge as service_charge')
            ->paginate(10);

            return response()->json($data);
        }
        return response()->json(['success'=>false, 'message'=>'only staff can update service charge data'], 202);
    }
}
