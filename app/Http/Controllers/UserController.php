<?php

namespace App\Http\Controllers;

use App\Mail\RegistrationMail;
use App\Models\Property;
use App\Models\ServiceRequest;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    

     public function registration(Request $request) {
        $adminDefaultGroupId = 3;
        $ownerDefaultGroupId = 2;
        $staffDefaultGroupId = 1;
        $validation = Validator::make($request->all(), [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'type' => 'required',
            'date_of_birth'=> 'required',
            'contact' => 'required',
            'address' => 'required',
            'city' => 'required',
            'landmark' => 'required',
            'state' => 'required',
            'pincode' => 'required'
        ]);

        if($validation->fails()) {
            return response()->json($validation->errors(), 402);
        }

        $userByEmail = User::where('email', '=', $request->email)->get();
        if(sizeof($userByEmail)) {
            return response()->json(['success'=>false,'message'=>'user with email already present'], 200);
        }

        $allData = $request->all();
        $allData['password'] = bcrypt($allData['password']);

        $user = User::create($allData);

        $resArr = [];
        $resArr['token'] = $user->createToken('housekeeping-application')->accessToken;
        $resArr['fname'] = $user->fname;

        // $email = $request->email;
       // Mail::to($email)->send(new RegistrationMail($allData));
       $userGroup = new UserGroup();
       $userGroup->user_id = $user->id;
       if($user->type == 2) {
            //admin
            $userGroup->group_id = $adminDefaultGroupId;
       } else if($user->type == 1) {
            //owner
            $userGroup->group_id = $ownerDefaultGroupId;
       } else {
            //staff
            $userGroup->group_id = $staffDefaultGroupId;
        }
       $userGroup->save();

        return response()->json(['success'=>true, 'data'=>$resArr], 200);
    }

     public function login(Request $request) {
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            /** @var \App\Models\User $user **/
            $user = Auth::user();
            if($user->blocked == 0) {
                $resArr = [];
                $resArr['token'] = $user->createToken('housekeeping-application')->accessToken;
                $resArr['fname'] = $user->fname;
                $userRights = $this->findRightsOfUser();
                $resArr['userRights'] = $userRights;

                return response()->json(['success'=>true, 'data'=>$resArr], 200);  
            } else {
                return response()->json(['success'=>false,'message'=>'Your account has been blocked, please contact the admin'], 200);
            }     
        } else {
            return response()->json(['success'=>false,'message'=>'Username and password does not match'], 402);
        }
    } 

    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], 202); 
    } 

    public function getProperties($limit) {
        $default = 10;
        $maxLimit = 200;
        if(!is_numeric($limit)) {
            $limit = $default;
        } else {
            if($limit > 200) {
                $limit = $maxLimit;
            }
            if($limit <= 0) {
                $limit =  $default;
            }
        }
        $user = Auth::user();
            $id = $user->id;
            return User::find($id)->getProperties()->where('archive', '=', 0)->paginate($limit);
    }

    public function logout(){ 
        if(Auth::check()) {
            $user = Auth::user();
            $user->aauthAcessToken()->delete();
            return "logout successfully";
        }
    }

    public function getStaff() {
        $user = Auth::user();
        $staff = User::where(['type'=>0, 'archive'=>0])->paginate(10);
        if($staff) {
            return response()->json($staff, 202);
        }
        return response()->json($staff, 202);
    }

    public function update(Request $request, $id) {
        $user = Auth::user();
        if($user->type == 2 || $user->id == $id) {
            $user = User::find($id);
            if($user) {
                $user->update($request->all());
                return $this::responseFormat($user, "user with id " . $id . " updated successfully", "user with id " . $id . " updation failed");
            } else {
                return response()->json(['success'=>false, 'message'=>'user not present for given id'], 202); 
            }
        } else {
            return response()->json(['success'=>false, 'message'=>'forbidden access to update'], 202); 
        }
    }

    public function index()
    {
        $user = Auth::user();
        $users = User::where('archive', '=', 0)->paginate(10);
        return response()->json($users);
    }

    public function getOwner() {
        $user = Auth::user();
        if($user->type == 2) {
            $owner = User::where(['type'=>1, 'archive'=>0])->paginate(10);
            if($owner) {
                return response()->json($owner, 202);
            }
        } else {
            return response()->json(['success'=>false, 'message'=>'only admin can access owner list'], 202); 
        }
    }

    public function destroy($id) {
        $user = Auth::user();
        if($user->type == 2) {
            $user = User::find($id);
            $user->archive = 1;
            $user->save();
            return ServiceController::responseFormat($user, "user with id " . $id . " deleted successfully", "user with id " . $id . " deletion failed");
        } else {
            return response()->json(['success'=>false, 'message'=>'only admin can delete user'], 202); 
        }
    }

    public function blocked($id) {
        $user = Auth::user();
        if($user->type == 2) {
            $user = User::find($id);
            $user->blocked = 1;
            $user->save();
            return $this::responseFormat($user, "user with id " . $id . " blocked successfully", "user with id " . $id . " blocking failed");
        } else {
            return response()->json(['success'=>false, 'message'=>'only admin can block user'], 202); 
        }
    }

    public function unblocked($id) {
        $user = Auth::user();
        if($user->type == 2) {
            $user = User::find($id);
            $user->blocked = 0;
            $user->save();
            return $this::responseFormat($user, "user with id " . $id . " unblocked successfully", "user with id " . $id . " unblocking failed");
        } else {
            return response()->json(['success'=>false, 'message'=>'only admin can unblock user'], 202); 
        }
    }

    public function userRights() {
        $rights = $this->findRightsOfUser();
        if($rights) {
            return response()->json(['success'=>true, 'message'=>'user rights fetch successfully', 'data'=>$rights], 202); 
        } else {
            return response()->json(['success'=>false, 'message'=>'fetching user rights failed', 'data'=>null], 202); 
        }
    }

    public function findRightsOfUser() {
        $user = Auth::user();
        $loggedInUserId = $user->id;
        $user = User::find($loggedInUserId);
        $groups = $user->groups;
        $res = [];
        foreach($groups as $group) {
           $rights = $group->rights->toArray();
           $res = array_merge($res, $rights);
            // echo gettype($rights);
        }
        return $res;
    }

    public function statisticsData($userType) {
        $resArr1 = [];
        $resArr2 = [];
        $resArr3 = [];
        $resArr4 = [];
        $resArr5 = [];
        $resArr = [];
        
        $user = Auth::user();
        $loggedInUserId = $user->id;
        if($userType == 2) {
            //admin
            $staff = User::where(['type'=>0, 'archive'=>0, 'blocked'=>0])->get();
            $staffCount = $staff->count();

            $owner = User::where(['type'=>1, 'archive'=>0, 'blocked'=>0])->get();
            $ownerCount = $owner->count();

            $properties = Property::where(['archive'=>0])->get();
            $propertiesCount = $properties->count();

            $serviceRequestsCmpl = ServiceRequest::where(['status'=>4]);
            $serviceRequestsCmplCount = $serviceRequestsCmpl->count();

            $serviceRequestsPending = ServiceRequest::where('status', '0')->orWhere('status', '1')->orWhere('status', '3');
            $serviceRequestsPendingCount = $serviceRequestsPending->count();

            // $resArr1['Total number of staffs available'] = $staffCount;
            $resArr1['name'] = 'Staffs Available';
            $resArr1['value'] = $staffCount;
            // $resArr2['Total number of owners available'] = $ownerCount;
            $resArr2['name'] = 'Owners Available';
            $resArr2['value'] = $ownerCount;
            // $resArr3['Total Properites'] = $propertiesCount;
            $resArr3['name'] = 'Properites Available';
            $resArr3['value'] = $propertiesCount;
            // $resArr4['Completed service requests count'] = $serviceRequestsCmplCount;
            $resArr4['name'] = 'Completed Service Requests';
            $resArr4['value'] = $serviceRequestsCmplCount;
            // $resArr5['In progress/new service requests count'] = $serviceRequestsPendingCount;
            $resArr5['name'] = 'New Service Requests';
            $resArr5['value'] = $serviceRequestsPendingCount;

            $resArr = array($resArr1, $resArr2, $resArr3, $resArr4, $resArr5);
        } else if($userType == 1) {
            //owner
            $properties = Property::where(['archive'=>0, 'user_id'=>$loggedInUserId])->get();
            $propertiesCount = $properties->count();

            $serviceRequests = ServiceRequest::join('properties', 'properties.id', '=', 'service_requests.properties_id')->where(['user_id'=>$loggedInUserId, 'archive'=>0]);
            $serviceRequestCount = $serviceRequests->count();

            $serviceRequestsRej = ServiceRequest::join('properties', 'properties.id', '=', 'service_requests.properties_id')->where(['user_id'=>$loggedInUserId, 'archive'=>0, 'service_requests.status'=>2]);
            $serviceRequestRejCount = $serviceRequestsRej->count();

            $serviceRequestsAcc = ServiceRequest::join('properties', 'properties.id', '=', 'service_requests.properties_id')->where(['user_id'=>$loggedInUserId, 'archive'=>0, 'service_requests.status'=>1]);
            $serviceRequestAccCount = $serviceRequestsAcc->count();
            
            // $resArr1['Total Properites'] = $propertiesCount;
            $resArr1['name'] = 'Properites Available';
            $resArr1['value'] = $propertiesCount;
            // $resArr2['Total service requests made'] = $serviceRequestCount;
            $resArr2['name'] = 'Service Requests';
            $resArr2['value'] = $serviceRequestCount;
            // $resArr3['Rejected service requests count'] = $serviceRequestRejCount;
            $resArr3['name'] = 'Rejected Service Requests';
            $resArr3['value'] = $serviceRequestRejCount;
            // $resArr4['Accepted service requests count'] = $serviceRequestAccCount;
            $resArr4['name'] = 'Accepted Service Requests';
            $resArr4['value'] = $serviceRequestAccCount;

            $resArr = array($resArr1, $resArr2, $resArr3, $resArr4);

            
        } else {
            //staff
            $serviceRequestsNew = ServiceRequest::where(['assigned_user'=>$loggedInUserId, 'service_requests.status'=>0]);
            $serviceRequestNewCount = $serviceRequestsNew->count();

            $serviceRequestsCmpl = ServiceRequest::where(['assigned_user'=>$loggedInUserId, 'service_requests.status'=>4]);
            $serviceRequestCmplCount = $serviceRequestsCmpl->count();

            $serviceRequestsRej = ServiceRequest::where(['assigned_user'=>$loggedInUserId, 'service_requests.status'=>2]);
            $serviceRequestRejCount = $serviceRequestsRej->count();

            $serviceRequestsAcc = ServiceRequest::where(['assigned_user'=>$loggedInUserId, 'service_requests.status'=>1]);
            $serviceRequestAccCount = $serviceRequestsAcc->count();

            // $resArr1['New service requests count '] = $serviceRequestNewCount;
            $resArr1['name'] = 'New Service Requests';
            $resArr1['value'] = $serviceRequestNewCount;
            // $resArr2['Completed service requests made'] = $serviceRequestCmplCount;
            $resArr2['name'] = 'Completed Service Requests';
            $resArr2['value'] = $serviceRequestCmplCount;
            // $resArr3['Rejected service requests count'] = $serviceRequestRejCount;
            $resArr3['name'] = 'Rejected Service Requests';
            $resArr3['value'] = $serviceRequestRejCount;
            // $resArr4['Accepted service requests count'] = $serviceRequestAccCount;
            $resArr4['name'] = 'Accepted Service Requests';
            $resArr4['value'] = $serviceRequestAccCount;

            $resArr = array($resArr1, $resArr2, $resArr3, $resArr4);

        }
        // return $resArr;
        return response()->json(['success'=>true, 'message'=>'statistics data fetch successfully', 'data'=>$resArr], 202); 

    }
 


}
