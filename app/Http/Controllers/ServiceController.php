<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
  
    public function index()
    {
        $user = Auth::user();
        if($user->type == 0 || $user->type == 1) {
            $services = Service::all();
            return $this::responseFormat($services, "service data fetched successfully", "service data not present");
        }
    }


    public function store(Request $request)
    {
        $user = Auth::user();
        if($user->type == 2 || $user->type == 0) {
            $request->validate([
                'name'=>'required'
            ]);

            $service = Service::create($request->all());
            return $this::responseFormat($service, "service created successfully", "service not created");
        }
        return response()->json(['success'=>false, 'message'=>'only admin and staff can store service list'], 202); 
    }

    
    public function show($id)
    {
        $user = Auth::user();
        if($user->type == 2) {
            $service = Service::find($id);
            return $this::responseFormat($service, "service with id " . $id . " fetched successfully", "service with id " . $id . " not present");
        }
        return response()->json(['success'=>false, 'message'=>'only admin can retrieve service data'], 202);
    }

    
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if($user->type == 2 || $user->type == 0) {
            $service = Service::find($id);
            if($service) {
                $service->update($request->all());
                return $this::responseFormat($service, "service with id " . $id . " updated successfully", "service with id " . $id . " updation failed");
            }
            return response()->json(['success'=>false,'message'=>"Service not present for the given id", 'data'=>'null'], 200);
        }
        return response()->json(['success'=>false, 'message'=>'only admin and staff can update service data'], 202);
    }


    public function destroy($id)
    {
        $user = Auth::user();
        if($user->type == 2 || $user->type == 0) {
            $service = Service::destroy($id);
            return $this::responseFormat($service, "service with id " . $id . " deleted successfully", "service with id " . $id . " deletion failed");
        }
        return response()->json(['success'=>false, 'message'=>'only owner can delete service data'], 202);
    }

}
