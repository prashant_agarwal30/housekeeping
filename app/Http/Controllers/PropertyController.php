<?php

namespace App\Http\Controllers;

use App\Models\Property;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Test\Constraint\ResponseFormatSame;
use App\Http\Controllers\ServiceController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PropertyController extends Controller
{
    
    public function index()
    {
        $properties = Property::where("archive", '=', 0)->get();
        return $this::responseFormat($properties, "all properties fetched successfully", "no properties present");
    }

   
    public function store(Request $request)
    {
        $loggedInUser = Auth::user();
            $request->validate([
            "address"=>"required",
            "name"=>"required",
            "status"=>"required", 
            "pincode"=>"required"
            ]);

            $property = new Property();
            //make first letter of word in capital
            $property->address = Str::ucfirst($request->address);
            $property->name = Str::ucfirst($request->name);
            $property->status = Str::ucfirst($request->status);
            $property->pincode = $request->pincode;
            $property->user_id = $loggedInUser->id;
            $property->save();
            
            return $this::responseFormat($property, "Property Created Successfully", "Property Not Created");
    }

    public function show($id)
    {   $loggedInUser = Auth::user();
        $property = Property::find($id);
        if($property) {
            $loggedInUser = Auth::user();
            if($property->user_id === $loggedInUser->id) {
                return $this::responseFormat($property, "property with id " . $id . " fetched successfully", "property with id " . $id . " not present");
            }
            return response()->json(['success'=>false,'message'=>"restricted access for updation of given resource"], 404);
        } else {
            return response()->json(['success'=>false,'message'=>"Property not present for the given id", 'data'=>'null'], 200);
        }

    }  


    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $property = Property::find($id);
        if($property){
            $userId = $property->user_id;
            //user is allowed to edit his property only
            if($userId === $user->id) {
                $property->update($request->all());
                return $this::responseFormat($property, "property with id " . $id . " updated successfully", "property with id " . $id . " updation failed");
            } else {
                return response()->json(['success'=>false,'message'=>"restricted access for updation of given resource"], 404); 
            }
        }
        return response()->json(['success'=>false,'message'=>"Property not present for the given id", 'data'=>'null'], 200);
    }


    public function destroy($id)
    {
        $user = Auth::user();
        $property = Property::find($id);
        if($property) {
            $userId = $property->user_id;
            if($userId === $user->id) {
                $property = Property::find($id);
                $property->archive = 1;
                $property->save();
                return ServiceController::responseFormat($property, "property with id " . $id . " deleted successfully", "property with id " . $id . " deletion failed");
            } else {
                return response()->json(['success'=>false,'message'=>"restricted access for deletion of given resource"], 404);
            }
        }
            return response()->json(['success'=>false,'message'=>"property not present for the given id", 'data'=>'null'], 200);
    }
}
