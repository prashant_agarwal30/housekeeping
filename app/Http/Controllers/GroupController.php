<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupRight;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::paginate(10);
        // return $this::responseFormat($groups, "all groups fetched successfully", "no groups present");
        return response()->json($groups);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "description"=>"required"
        ]);
        $input = $request->all();
        if(!empty($input)) {
            $group = new Group();
            $group->name = isset($input['name']) ? $input['name'] : '' ;
            $group->description = isset($input['description']) ? $input['description'] : '';
            $group->save();

            return $this::responseFormat($group, "group saved successfully", "group saving failed");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!empty($request->all()) && $id > 0) {
            $group = Group::find($id);
            if($group) {
                $group->update($request->all());
                return $this::responseFormat($group, "group with id " . $id . " updated successfully", "group with id " . $id . " updation failed");
            } else {
                return response()->json(['success'=>false,'message'=>"group not present for the given id", 'data'=>'null'], 200);
            }
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id > 0) {
            $res=Group::where('id',$id)->delete();
            if($res) {
                return response()->json(['success'=>true,'message'=>"group deleted successfully"], 200);
            } else {
                return response()->json(['success'=>false,'message'=>"group deletion failed"], 200);
            }
        }
    }

    public function store_users(Request $request) {
        $inputs = $request->all();
        if(!empty($inputs)){
            $request->validate([
                "group_id"=>"required"
            ]);
            $groupId = isset($inputs['group_id']) ? $inputs['group_id'] : '';
            $users = isset($inputs['users']) ? $inputs['users'] : '';
            $rights = isset($inputs['rights']) ? $inputs['rights'] : '';

            UserGroup::where('group_id', $groupId)->delete();

            foreach($users as $user) {
                $userGroup = new UserGroup();
                $userGroup->user_id = $user;
                $userGroup->group_id = $groupId;
                $userGroup->save();
            }

            GroupRight::where('group_id', $groupId)->delete();

            foreach($rights as $right) {
                $groupRight = new GroupRight();
                $groupRight->group_id = $groupId;
                $groupRight->right_id = $right;
                $groupRight->save();
            }
    
            return response()->json(['success'=>true, 'message'=>'users group and rights saved successfully'], 202); 
        }
       
    }


    public function getUsersByGroupId($id) {
        $group = Group::find($id);
        if($group) {
            $users = $group->users;
            return response()->json(['message'=>" users for group_id fetched successfully ", 'data'=>$users], 200);
        } else {
            return response()->json(['message'=>"group not present for the given id", 'data'=>'null'], 200);
        }
    }

    public function getGroupsByUserId($id) {
        $user = User::find($id);
        if($user) {
            $groups = $user->groups;
            // return $groups;
            return response()->json(['message'=>" users for group_id fetched successfully ", 'data'=>$groups], 200);

        }
    }
}
